package com.luxoft.demo;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CsvData {

    @Id
    private String primaryKey;
    private String name;
    private String description;
    private LocalDateTime updatedTimestamp;

}
