package com.luxoft.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/data")
public class CsvDataController {

    private final CsvDataRepository dataRepository;

    public CsvDataController(CsvDataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    @PostMapping("/upload")
    public ResponseEntity<String> uploadData(@RequestParam("file") MultipartFile file) throws Exception {
        String data = new String(file.getBytes());
        String[] lines = data.split("\n");
        for (int i = 1; i < lines.length - 1; i++) {
            String[] fields = lines[i].split(",");
            validateFields(fields);
            CsvData newData = createData(fields);
            dataRepository.save(newData);
        }
        return ResponseEntity.ok("Data uploaded successfully.");
    }

    @GetMapping("/{primaryKey}")
    public ResponseEntity<CsvData> getData(@PathVariable String primaryKey) {
        CsvData data = dataRepository.findById(primaryKey).orElse(null);
        if (data == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(data);
    }

    @DeleteMapping("/{primaryKey}")
    public ResponseEntity<String> deleteData(@PathVariable String primaryKey) {
        dataRepository.deleteById(primaryKey);
        return ResponseEntity.ok("Data deleted successfully.");
    }

    private void validateFields(String[] fields) throws Exception {
        if (fields.length != 4) {
            throw new Exception("Invalid number of fields.");
        }
        if (fields[0].isEmpty()) {
            throw new Exception("PRIMARY_KEY cannot be empty.");
        }
        if (!fields[3].isEmpty() && !isValidTimestamp(fields[3])) {
            throw new Exception("Invalid timestamp format.");
        }
    }

    private CsvData createData(String[] fields) {
        return new CsvData(fields[0], fields[1], fields[2], LocalDateTime.parse(fields[3]));
    }

    private boolean isValidTimestamp(String timestamp) {
        try {
            DateTimeFormatter.ISO_DATE_TIME.parse(timestamp);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
